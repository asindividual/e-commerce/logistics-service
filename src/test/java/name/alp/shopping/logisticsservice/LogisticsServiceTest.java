package name.alp.shopping.logisticsservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = LogisticsRestController.class)
@Import(LogisticsService.class)
public class LogisticsServiceTest {

    @Autowired
    private  LogisticsService logisticsService;


    @MockBean
    LogisticsRepository repository;

    @Test
    public void saveTest(){
        Logistics logistics = new Logistics();
        logistics.setId("test_id1");
        logistics.setCompanyName("Test Company");
        Mockito.when(repository.save(logistics)).thenReturn(Mono.just(logistics));

        Mono<Logistics> result =  logisticsService.save(logistics);

        assertThat(result).isNotNull();
    }

    @Test
    public void findByIdTest(){
        Logistics logistics = new Logistics();
        logistics.setId("test_id1");
        logistics.setCompanyName("Test Company");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(logistics));

        Mono<Logistics> result =  logisticsService.findById("test_id1");
        assertThat(result).isNotNull();
    }

    @Test
    public void logisticsByCargoTest(){
        Logistics logistics = new Logistics();
        logistics.setId("test_id1");
        logistics.setCompanyName("Test Company");


        String fromCity="Istanbul";
        String toCity="Ankara";
        Mockito
                .when(repository.logisticsByCargo(fromCity,toCity,100d))
                .thenReturn(Flux.fromArray(new Logistics[]{logistics}));

        Flux<Logistics> result = logisticsService.findByCargo(fromCity, toCity, 100d);
        assertThat(result.collectList().block().size()).isEqualTo(1);
    }





}
