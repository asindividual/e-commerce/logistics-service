package name.alp.shopping.logisticsservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = LogisticsRestController.class)
@Import(LogisticsService.class)
public class LogisticsRestControllerTest {

    @MockBean
    LogisticsRepository repository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void testSave() {
        Logistics logistics = new Logistics();
        logistics.setId("test_id1");
        logistics.setCompanyName("Test Company");


        Mockito.when(repository.save(logistics)).thenReturn(Mono.just(logistics));

        webClient.post()
                .uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(logistics))
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void testFindById()
    {
        Logistics logistics = new Logistics();
        logistics.setId("test_id1");
        logistics.setCompanyName("Test Company");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(logistics));

        webClient.get().uri("/{id}", "test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.companyName").isNotEmpty()
                .jsonPath("$.id").isEqualTo("test_id1");

        Mockito.verify(repository, times(1)).findById("test_id1");
    }

    @Test
    public void logisticsByCargoTest(){
        Logistics logistics = new Logistics();
        logistics.setId("test_id1");
        logistics.setCompanyName("Test Company");


        String fromCity="Istanbul";
        String toCity="Ankara";

        Mockito
                .when(repository.logisticsByCargo(fromCity,toCity,100d))
                .thenReturn(Flux.fromArray(new Logistics[]{logistics}));


        webClient.get().uri("/fromCity/{fromCity}/toCity/{toCity}/totalDeci/{totalDeci}",fromCity,toCity,100d)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Logistics.class);

        Mockito.verify(repository, times(1)).logisticsByCargo(fromCity,toCity,100d);


    }

}
