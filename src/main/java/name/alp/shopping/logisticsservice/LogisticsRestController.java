package name.alp.shopping.logisticsservice;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class LogisticsRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogisticsRestController.class);
    private final LogisticsService logisticsService;

    public LogisticsRestController(LogisticsService logisticsService) {
        this.logisticsService = logisticsService;
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates" )
    @PostMapping
    public Mono<Logistics> save(@RequestBody Logistics logistics) {
        LOGGER.info("create: {}", logistics);
        return logisticsService.save(logistics);
    }

    @Operation(summary = "Provides an entity for the specified id")
    @GetMapping("/{id}")
    public Mono<Logistics> findById(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return logisticsService.findById(id);
    }

    @Operation(summary = "List Logstic Companies for the given fromCity,toCity and total amount of the cargo totalDeci")
    @GetMapping("/fromCity/{fromCity}/toCity/{toCity}/totalDeci/{totalDeci}")
    public Flux<Logistics> findByCargo(@PathVariable("fromCity") String fromCity, @PathVariable("toCity") String toCity , @PathVariable("totalDeci") Double totalDeci) {
        LOGGER.info("findByCargo:  {} {} {}", fromCity,toCity,totalDeci);
        return logisticsService.findByCargo(fromCity,toCity,totalDeci);
    }

}
