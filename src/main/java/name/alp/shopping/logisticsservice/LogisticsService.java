package name.alp.shopping.logisticsservice;

import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class LogisticsService {

    private final LogisticsRepository logisticsRepository;

    public LogisticsService(LogisticsRepository logisticsRepository) {
        this.logisticsRepository = logisticsRepository;
    }

    public Mono<Logistics> save(Logistics logistics) {
        return logisticsRepository.save(logistics);
    }

    public Mono<Logistics> findById(String id) {
        return logisticsRepository.findById(id);
    }

    public Flux<Logistics> findByCargo(String fromCity, String toCity , Double totalDeci){
        return logisticsRepository.logisticsByCargo(  fromCity,   toCity ,   totalDeci);
    }

}
