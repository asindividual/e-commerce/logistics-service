package name.alp.shopping.logisticsservice;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface LogisticsRepository extends ReactiveMongoRepository<Logistics ,String> {
    @Query("{ " +
                " 'fromCities': ?#{[0]}  , " +
                " 'toCities'  : ?#{[1]}  ," +
                " 'maxDeci' : { '$gte' : ?#{[2]}  }" +
            " }")
    Flux<Logistics> logisticsByCargo(String fromCity, String toCity , Double totalDeci);
}
